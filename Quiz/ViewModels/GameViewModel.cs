﻿using Quiz.Helpers;
using Quiz.Models;
using Quiz.Networks;
using Quiz.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.IO.Packaging;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Quiz.ViewModels
{
    public class GameViewModel : BaseViewModel
    {
        public static int role = -1;

        Question currentQuestion;
        int score;
        string scoreText;

        bool canNextQuestion;
        bool canPoint;
        bool canValidate = true;
        string themeName;
        string answer;
        int max;

        public Question CurrentQuestion { get => currentQuestion; set => OnPropertyChanged(ref currentQuestion, value); }
        public string Score { get => scoreText; set => OnPropertyChanged(ref scoreText, value); }
        public int Role { get; set; }
        public string Pseudo { get; set; }
        public string Answer { get => answer; set => OnPropertyChanged(ref answer, value); }
        public int QuestionsCount { get; set; }
        public int QuestionNumber { get; set; }

        public bool Finish { get; private set; }

        public ICommand NextQuestionCommand { get; set; }
        public ICommand PointCommand { get; set; }
        public ICommand ValidateCommand { get; set; }
        public ICommand ShowAnswerCommand { get; set; }

        Queue<Question> questions;

        public GameViewModel(List<Question> list, string pseudo, string themeName)
        {
            questions = new Queue<Question>(list);
            QuestionsCount = questions.Count;
            Pseudo = pseudo;
            this.themeName = themeName;
            Role = role;
            max = list.Count;

            Network.OnReceivedPacket<ValidatePacket>(OnReceivedValidate);
            Network.OnReceivedPacket<ScorePacket>(OnReceivedScore);
            Network.OnReceivedPacket<NextQuestionPacket>(OnReceivedNextQuestion);

            NextQuestionCommand = new RelayCommand(() =>
            {
                var request = Role == 1 ? true : false;
                Network.SendPacket(new NextQuestionPacket(request), !request);
                canNextQuestion = false;
            }, () => canNextQuestion && questions.Count != 0);

            NextQuestion();

            PointCommand = new RelayCommand<string>((point) =>
            {
                var delta = int.Parse(point);
                Network.SendPacket(new ScorePacket(delta), true);
                canNextQuestion = true;
                if (Role == 0)
                    PauseMusic();

            }, (_) => canPoint);

            ValidateCommand = new RelayCommand(() =>
            {
                Network.SendPacket(new ValidatePacket());
                canValidate = false;
                PauseMusic();
            }, () => canValidate);

            ShowAnswerCommand = new RelayCommand(() => Answer = currentQuestion.name);
        }

        void OnReceivedValidate(ValidatePacket packet)
        {
            canPoint = true;
            Utility.RaiseCanExecuteChanged();
            PauseMusic();
        }

        void OnReceivedScore(ScorePacket packet)
        {
            score += packet.delta;
            var num = packet.delta > 0 ? "+" : "";
            Score = $"{score} ({num}{packet.delta})";
            canPoint = false;
            if (questions.Count == 0)
                Finish = true;
            Utility.RaiseCanExecuteChanged();

            if (questions.Count == 0 &&
                ((!Network.IsValid && MessageBox.Show("Ajouter au score ?", "Score", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                || Network.IsValid))
            {
                ScoreViewModel.Add(Pseudo, themeName, score, max);
            }
        }

        void OnReceivedNextQuestion(NextQuestionPacket packet)
        {
            if (packet.request)
            {
                canNextQuestion = true;
                Utility.RaiseCanExecuteChanged();
            }
            else
                NextQuestion();
        }

        void NextQuestion()
        {
            CurrentQuestion = questions.Dequeue();
            QuestionNumber++;
            OnPropertyChanged(nameof(QuestionNumber));
            canPoint = Role == 0 ? true : false;
            canNextQuestion = false;
            canValidate = true;
            Score = score.ToString();
            Answer = null;
            Utility.RaiseCanExecuteChanged();
        }

        void PauseMusic()
        {
            if (CurrentQuestion is MusicQuestion music)
                Application.Current.Dispatcher.Invoke(music.Pause);
        }
    }
}
