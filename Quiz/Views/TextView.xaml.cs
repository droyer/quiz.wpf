﻿using Quiz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz.Views
{
    public partial class TextView : UserControl
    {
        Window window;

        public TextView()
        {
            InitializeComponent();

            Loaded += TextView_Loaded;
            Unloaded += TextView_Unloaded;
            DataContextChanged += TextView_DataContextChanged;
        }

        private void TextView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            UpdateMaxWidth();
        }

        private void TextView_Loaded(object sender, RoutedEventArgs e)
        {
            window = Window.GetWindow(this);

            window.SizeChanged += Window_SizeChanged;
            window.StateChanged += Window_StateChanged;

            UpdateMaxWidth();
        }

        private void TextView_Unloaded(object sender, RoutedEventArgs e)
        {
            window.SizeChanged -= Window_SizeChanged;
            window.StateChanged -= Window_StateChanged;
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateMaxWidth();
        }

        private void Window_StateChanged(object sender, EventArgs e)
        {
            UpdateMaxWidth();
        }

        void UpdateMaxWidth()
        {
            if (DataContext is TextQuestion viewModel && window != null)
                viewModel.MaxWidth = window.ActualWidth / 2;
        }
    }
}
