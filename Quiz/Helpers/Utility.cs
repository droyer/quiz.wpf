﻿using Quiz.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Quiz.Helpers
{
    public static class Utility
    {
        public readonly static string applicationDirectory;

        static Random random;

        static Utility()
        {
            random = new Random();
            applicationDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        }

        public static void RaiseCanExecuteChanged()
        {
            if (Application.Current.Dispatcher.Thread == Thread.CurrentThread)
                CommandManager.InvalidateRequerySuggested();
            else
                Application.Current.Dispatcher.Invoke(CommandManager.InvalidateRequerySuggested);
        }

        public static IList<T> Shuffle<T>(IList<T> list)
        {
            var newList = new List<T>();

            int count = list.Count;
            for (int i = 0; i < count; i++)
            {
                int index = random.Next(list.Count);
                newList.Add(list[index]);
                list.RemoveAt(index);
            }

            return newList;
        }

        public static void Randomize<T>(this IList<T> list) where T : Question
        {
            int max = list.Count;

            var questions = new List<Queue<T>>();

            questions.Add(new Queue<T>(Shuffle(list.Where(q => q is MusicQuestion).ToList())));
            questions.Add(new Queue<T>(Shuffle(list.Where(q => q is TextQuestion).ToList())));
            questions.Add(new Queue<T>(Shuffle(list.Where(q => q is ImageQuestion).ToList())));

            questions = questions.OrderBy(l => l.Count).ToList();

            for (int i = 0; i < max; i++)
                list[i] = null;

            for (int i = 0; i < 2; i++)
            {
                if (questions[i].Count == 0)
                    continue;

                int num = max / questions[i].Count;
                if (num == max)
                    num = max / 2 + 1;
                int j = num - 1;

                while (questions[i].Count > 0)
                {
                    if (list[j] != null)
                    {
                        j++;
                        if (j >= max)
                            j = 0;
                    }

                    list[j] = questions[i].Dequeue();

                    j += num;
                    if (j >= max)
                        j = j - max;
                }
            }

            for (int i = 0; i < max; i++)
            {
                if (list[i] == null)
                    list[i] = questions[2].Dequeue();
            }
        }
    }
}
