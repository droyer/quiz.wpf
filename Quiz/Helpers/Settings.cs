﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Quiz.Helpers
{
    public static class Settings
    {
        public readonly static string file = Utility.applicationDirectory + "\\Settings.ini";

        public static string Pseudo { get; private set; }
        public static string Ip { get; private set; } = "localhost";
        public static int Port { get; private set; } = 1995;
        public static int Volume { get; private set; } = 50;

        public static bool Read()
        {
            try
            {
                string text = File.ReadAllText(file);
                var lines = text.Split(new[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var line in lines)
                {
                    var args = line.Split(new[] { '=' }, 2);
                    switch (args[0])
                    {
                        case nameof(Pseudo):
                            Pseudo = args[1].Trim();
                            break;
                        case nameof(Ip):
                            Ip = args[1].Trim();
                            break;
                        case nameof(Port):
                            Port = int.Parse(args[1].Trim());
                            break;
                        case nameof(Volume):
                            Volume = int.Parse(args[1].Trim());
                            break;
                    }
                }

                if (string.IsNullOrWhiteSpace(Pseudo) || string.IsNullOrWhiteSpace(Ip) || Port == 0)
                    return false;

                return true;
            }
            catch { }

            return false;
        }

        public static void Write()
        {
            try
            {
                using (var sw = new StreamWriter(file))
                {
                    sw.WriteLine($"{nameof(Pseudo)}={Pseudo}");
                    sw.WriteLine($"{nameof(Ip)}={Ip}");
                    sw.WriteLine($"{nameof(Port)}={Port}");
                    sw.WriteLine($"{nameof(Volume)}={Volume}");
                }
            }
            catch { }
        }
    }
}
