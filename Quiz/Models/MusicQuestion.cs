﻿using Quiz.Helpers;
using Quiz.ViewModels;
using Quiz.Views;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace Quiz.Models
{
    public class MusicQuestion : Question
    {
        public static int currentVolume = Settings.Volume;

        const string playText = "\uF5B0";
        const string pauseText = "\uEDB4";

        string time = "--:--";
        double value;
        string length = "--:--";
        string buttonState = pauseText;
        int volume;
        string path;
        MediaElement player;
        DispatcherTimer timer;
        bool playing;

        public string Time { get => time; set => OnPropertyChanged(ref time, value); }
        public double Value { get => value; set => OnPropertyChanged(ref this.value, value); }
        public string Length { get => length; set => OnPropertyChanged(ref length, value); }
        public string ButtonState { get => buttonState; set => OnPropertyChanged(ref buttonState, value); }
        public int Volume { get => volume; set => OnPropertyChanged(ref volume, value); }

        public bool barClicked;

        public ICommand ChangeStateCommand { get; set; }
        public ICommand RestartCommand { get; set; }

        public MusicQuestion(byte[] buffer) : base(buffer)
        {
            string hash;

            using (var sha = SHA256.Create())
            {
                var roleBuffer = BitConverter.GetBytes(GameViewModel.role);
                var finalBuffer = new byte[buffer.Length + roleBuffer.Length];
                roleBuffer.CopyTo(finalBuffer, 0);
                buffer.CopyTo(finalBuffer, roleBuffer.Length);

                var bytes = sha.ComputeHash(finalBuffer);

                var builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                    builder.Append(bytes[i].ToString("x2"));

                hash = builder.ToString();
            }

            path = $"{Path.GetTempPath()}{hash}.mp3";

            Application.Current.Dispatcher.Invoke(() =>
            {
                timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromMilliseconds(200);
                timer.Tick += Timer_Tick;
            });

            ChangeStateCommand = new RelayCommand(Switch);

            RestartCommand = new RelayCommand(() =>
            {
                Play();
                player.Position = TimeSpan.Zero;
                Value = 0;
            });
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (player.NaturalDuration.HasTimeSpan)
            {
                if (!barClicked)
                {
                    double percent = (double)player.Position.Ticks / (double)player.NaturalDuration.TimeSpan.Ticks;
                    Value = Math.Ceiling(percent * 100.0);
                }

                Length = player.NaturalDuration.TimeSpan.ToString(@"mm\:ss");
            }

            if (!barClicked)
                UpdateTimeText(player.Position);
        }

        void Reset()
        {
            Value = 0;
            Time = "00:00";
        }

        public void UpdateTimeText(TimeSpan timeSpan)
        {
            Time = timeSpan.ToString(@"mm\:ss");
        }

        public void Init(MediaElement player)
        {
            this.player = player;

            timer.Start();

            File.WriteAllBytes(path, buffer);

            player.Source = new Uri(path);
            player.LoadedBehavior = MediaState.Manual;
            player.MediaEnded += Player_MediaEnded;
            SetVolume(currentVolume);
            player.Play();
            playing = true;
        }

        private void Player_MediaEnded(object sender, RoutedEventArgs e)
        {
            Pause();
            player.Position = TimeSpan.Zero;
            Value = 0;
        }

        public void Dispose()
        {
            timer.Stop();
            Application.Current.Dispatcher.Invoke(player.Close);
            Task.Run(() => File.Delete(path));
        }

        public void SetVolume(int newVolume)
        {
            currentVolume = newVolume;
            Volume = newVolume;

            var percent = newVolume / 100.0;
            if (percent == 1)
                percent = 0.99;
            player.Volume = percent;
        }

        public static void TryStop(object currentView)
        {
            if (currentView is GameViewModel game)
            {
                if (game.CurrentQuestion is MusicQuestion music)
                {
                    music.Dispose();
                }
            }
        }

        void Play()
        {
            player.Play();
            playing = true;
            ButtonState = pauseText;
        }

        public void Pause()
        {
            player.Pause();
            playing = false;
            ButtonState = playText;
        }

        void Switch()
        {
            if (playing)
                Pause();
            else
                Play();
        }
    }
}
