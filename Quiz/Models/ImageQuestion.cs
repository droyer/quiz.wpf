﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Quiz.Models
{
    public class ImageQuestion : Question
    {
        public BitmapImage Image { get; set; }

        public ImageQuestion(byte[] buffer) : base(buffer)
        {
            var memoryStream = new MemoryStream(buffer);

            Image = new BitmapImage();
            Image.BeginInit();
            Image.StreamSource = memoryStream;
            Image.EndInit();
            Image.Freeze();
        }
    }
}
