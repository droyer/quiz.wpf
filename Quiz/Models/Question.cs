﻿using Quiz.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz.Models
{
    public abstract class Question : BaseViewModel
    {
        public byte[] buffer { get; private set; }
        public string name { get; private set; }

        public Question(byte[] buffer)
        {
            this.buffer = buffer;
        }

        public static Question New(string name, byte[] buffer)
        {
            Question question = null;

            if (CheckExts(name, "jpg", "png", "jpeg"))
                question = new ImageQuestion(buffer);
            else if (CheckExts(name, "mp3", "wav"))
                question = new MusicQuestion(buffer);
            else if(CheckExts(name, "txt"))
                question = new TextQuestion(buffer);

            question.name = Path.GetFileNameWithoutExtension(name);
            return question;
        }

        static bool CheckExts(string name, params string[] exts)
        {
            foreach (var item in exts)
            {
                if (name.EndsWith($".{item}", true, CultureInfo.CurrentCulture))
                    return true;
            }

            return false;
        }
    }
}
