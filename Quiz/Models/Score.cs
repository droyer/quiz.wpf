﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Quiz.Models
{
    public class Score
    {
        public string Pseudo { get; set; }
        public string ThemeName { get; set; }
        public string Date { get; set; }
        public int Value { get; set; }
        public int Max { get; set; }
        public int Percent { get; set; }
        public Brush Color { get; set; }

        public override string ToString()
        {
            return $"{Pseudo} {ThemeName} {Date} {Value} ({Max}) {Percent}";
        }
    }
}
