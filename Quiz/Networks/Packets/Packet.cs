﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz.Networks
{
    public abstract class Packet
    {
        public abstract void Serialize(NetOutgoingMessage outmsg);
        public abstract void Deserialize(NetIncomingMessage inc);
    }
}
