﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;

namespace Quiz.Networks
{
    public class PseudoPacket : Packet
    {
        public string pseudo { get; private set; }

        public PseudoPacket(string pseudo)
        {
            this.pseudo = pseudo;
        }

        public PseudoPacket()
        {
        }

        public override void Serialize(NetOutgoingMessage outmsg)
        {
            outmsg.Write(pseudo);
        }

        public override void Deserialize(NetIncomingMessage inc)
        {
            pseudo = inc.ReadString();
        }
    }
}
