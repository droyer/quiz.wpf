﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;

namespace Quiz.Networks
{
    public class NewGamePacket : Packet
    {
        public string themeName { get; private set; }
        public int maxQuestions { get; private set; }

        public NewGamePacket(string themeName, int maxQuestions)
        {
            this.themeName = themeName;
            this.maxQuestions = maxQuestions;
        }

        public NewGamePacket() { }

        public override void Serialize(NetOutgoingMessage outmsg)
        {
            outmsg.Write(themeName);
            outmsg.Write(maxQuestions);
        }

        public override void Deserialize(NetIncomingMessage inc)
        {
            themeName = inc.ReadString();
            maxQuestions = inc.ReadInt32();
        }
    }
}
